Rails.application.routes.draw do
  root 'welcome#index'

  resources :posts
  get "/search" => "posts#search", as: :search_post

  resource  :session
  get "/:id" => "users#show", as: :user

  resources :stocks, only: %i(create destroy)
  get "/:id/stocks" => "stocks#index", as: :index_stock

  resources :comments, only: %i(create update destroy)

  get '/auth/google_oauth2/callback' => 'sessions#create_by_omniauth'
end
