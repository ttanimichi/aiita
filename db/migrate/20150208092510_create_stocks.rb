class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.references :user, null: false
      t.references :post, null: false

      t.timestamps null: false
    end

    add_index :stocks, :user_id
    add_index :stocks, :post_id
    add_index :stocks, [:user_id, :post_id], unique: true
  end
end
