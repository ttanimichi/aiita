class SessionsController < ApplicationController
  def create_by_omniauth
    env = request.env['omniauth.auth']
    user = User.find_or_initialize_by(uid: env[:uid])
    user.update!(
      uid:        env[:uid],
      name:       env[:info][:name],
      first_name: env[:info][:first_name],
      last_name:  env[:info][:last_name],
      email:      env[:info][:email],
      nickname:   env[:info][:email].split('@').first,
      image_url:  env[:info][:image]
    )
    session[:user_id] = user.id
    redirect_to root_url
  end

  def destroy
    destroy_current_user
    redirect_to root_path
  end
end
