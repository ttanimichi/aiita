class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  has_many :stocks, dependent: :destroy
  has_many :stocked_users, through: :stocks, class_name: 'User', source: :user
end
