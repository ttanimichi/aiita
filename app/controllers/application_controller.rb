class ApplicationController < ActionController::Base
  include Authorization

  protect_from_forgery with: :exception

  helper_method :current_user

  def current_user
    @currnet_user ||= User.find_by(id: session[:user_id])
  end

  def destroy_current_user
    @currnet_user = nil
    session[:user_id] = nil
  end
end
