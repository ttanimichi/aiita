module Authorization
  extend ActiveSupport::Concern

  def authorization_required
    redirect_to '/auth/google_oauth2' unless current_user
  end
end
