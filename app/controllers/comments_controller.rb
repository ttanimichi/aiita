class CommentsController < ApplicationController
  def create
    Comment.create!(comment_params)
    redirect_to :back
  end

  def update
  end

  def destroy
  end

  private

  def comment_params
    params.require(:comment).permit(:post_id, :body).merge(user: current_user)
  end
end
