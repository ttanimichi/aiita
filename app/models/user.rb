class User < ActiveRecord::Base
  has_many :posts
  has_many :stocks, dependent: :destroy
  has_many :stocked_posts, through: :stocks, class_name: 'Post', source: :post
end
