class PostsController < ApplicationController
  before_action :authorization_required, except: %i(index show)

  def show
    @post     = Post.find_by(id: params[:id]).decorate
    @stock    = Stock.find_by(user_id: current_user, post_id: @post)
    @comments = @post.comments
    @comment  = Comment.new
  end

  def new
    @post = Post.new
  end

  def create
    post = Post.create!(post_params)
    redirect_to post_url(post)
  end

  def edit
  end

  def update
  end

  def destroy
  end

  def search
  end

  private

  def post_params
    params.require(:post).permit(:title, :body).merge(user: current_user)
  end
end
