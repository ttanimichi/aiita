class StocksController < ApplicationController
  def index
    @posts = User.find_by(nickname: params[:id]).stocked_posts
  end

  def create
    @stock = Stock.find_or_create_by(user_id: current_user.id, post_id: params[:post_id])
  end

  def destroy
    stock = Stock.find_by(id: params[:id])
    @post = stock.post
    stock.destroy if stock.user == current_user
  end
end
