Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2,
  ENV["GOOGLE_CLIENT_ID"],    # TODO: prepare default account for Aiita
  ENV["GOOGLE_CLIENT_SECRET"] # TODO: prepare default account for Aiita
end
