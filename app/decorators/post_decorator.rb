class PostDecorator < Draper::Decorator
  delegate_all

  def to_html
    GitHub::Markdown.render_gfm(body).html_safe
  end
end
