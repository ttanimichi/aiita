class WelcomeController < ApplicationController
  def index
    @posts = Post.includes(:user).order("created_at DESC").limit(10)
  end
end
